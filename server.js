// we'll load up node's built in file system helper library here
// (we'll be using this later to serve our JSON files
const fs = require('fs')

// load up the express framework and body-parser helper
const express = require('express')
const bodyParser = require('body-parser')

const cors = require('cors')

const corsOptions = {
  origin: [
    'http://localhost:5000',
    'http://localhost:3001',
    'https://recruitment-task-help.herokuapp.com'
  ],
  optionsSuccessStatus: 200
}

// create an instance of express to serve our end points
const app = express()
// temporary for mobile browsers
app.use(cors())

// configure our express instance with some body-parser settings
// including handling JSON data
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// this is where we'll handle our various routes from
const routes = require('./routes/routes.js')(app, fs)

// finally, launch our server on port 3001.
const port = process.env.PORT || 3001
const server = app.listen(port, () => {
  console.log('listening on port %s...', server.address().port)
})
